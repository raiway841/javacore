import java.text.SimpleDateFormat;
import java.util.Date;

public class Exercise3 {
	public void question1(Exam exam) {
		for (int i = 0; i < 1; ++i) {
			System.out.println(exam.ExamID);
			System.out.println(exam.Code);
			System.out.println(exam.Title);
			System.out.println(exam.CategoryID);
			System.out.println(exam.Duration);
			System.out.println(exam.CreatorID);
		}
		String parttern = "dd/MM/yyyy";
		SimpleDateFormat formater = new SimpleDateFormat(parttern); 
		String kq = formater.format(exam.CreateDate);
		System.out.println(kq);
	}
	
	public void question2(Exam exam) {
		String parttern = "dd";
		SimpleDateFormat formater = new SimpleDateFormat(parttern); 
		String kq = formater.format(exam.CreateDate);
		System.out.println("Exam da tao vao ngay: " + kq);
	}
	
	public void question3(Exam exam) {
		String parttern = "yyyy";
		SimpleDateFormat formater = new SimpleDateFormat(parttern); 
		String kq = formater.format(exam.CreateDate);
		System.out.println("Exam da tao vao nam: " + kq);
	}
	
	public void question4(Exam exam) {
		String parttern = "MM/yyyy";
		SimpleDateFormat formater = new SimpleDateFormat(parttern); 
		String kq = formater.format(exam.CreateDate);
		System.out.println("Exam da tao vao: " + kq);
	}
	
	public void question5(Exam exam) {
		String parttern = "MM-DD";
		SimpleDateFormat formater = new SimpleDateFormat(parttern); 
		String kq = formater.format(exam.CreateDate);
		System.out.println("Exam da tao vao: " + kq);
	}

}

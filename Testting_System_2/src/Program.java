import java.util.Date;
import java.util.Scanner;

import javax.swing.event.SwingPropertyChangeSupport;

public class Program {
	public static void main(String[] args) {
		// DEPARTMENT
		Department department1 = new Department();
		Department department2 = new Department();
		Department department3 = new Department();
		Department[] departments = { department1, department2, department3 };
		department1.DepartmentID = 1;
		department1.departmentName = DepartmentName.Sale;

		department2.DepartmentID = 2;
		department2.departmentName = DepartmentName.Engineering;

		department3.DepartmentID = 3;
		department3.departmentName = DepartmentName.Marketting;

		// POSITION
		Position position1 = new Position();
		Position position2 = new Position();
		Position position3 = new Position();

		position1.positionID = 1;
		position1.positionName = PositionName.Dev;

		position2.positionID = 2;
		position2.positionName = PositionName.Test;
		position3.positionID = 3;
		position3.positionName = PositionName.PM;

		// ACCOUNT
		Account account1 = new Account();
		Account account2 = new Account();
		Account account3 = new Account();
		Account[] accounts = { account1, account2, account3 };

		account1.AccountID = 1;
		account1.Email = "account1@gmail.com";
		account1.Username = "acc1";
		account1.FullName = "account1";
		account1.DepartmentID = department3.DepartmentID;
		account1.PositionID = 3;
		account1.CreateDate = new Date();

		account2.AccountID = 2;
		account2.Email = "account2@gmail.com";
		account2.Username = "acc2";
		account2.FullName = "account2";
		account2.DepartmentID = department3.DepartmentID;
		account2.PositionID = 2;
		account2.CreateDate = new Date();

		account3.AccountID = 3;
		account3.Email = "account3@gmail.com";
		account3.Username = "acc3";
		account3.FullName = "account3";
		account3.DepartmentID = department1.DepartmentID;
		account3.PositionID = 1;
		account3.CreateDate = new Date();

		// GROUP
		Group group1 = new Group();
		Group group2 = new Group();
		Group group3 = new Group();

		group1.GroupID = 1;
		group1.GroupName = "Group 1";
		group1.CreatorID = account1.AccountID; // Group 1 creator by account 1
		group1.CreateDate = new Date();

		group2.GroupID = 2;
		group2.GroupName = "Group 2";
		group2.CreatorID = account3.AccountID; // Group 2 creator by account 3
		group2.CreateDate = new Date();

		group3.GroupID = 3;
		group3.GroupName = "Group 3";
		group3.CreatorID = account2.AccountID; // Group 3 creator by account 2
		group3.CreateDate = new Date();

		// GROUPACCOUNT
		GroupAccount ga1 = new GroupAccount();
		GroupAccount ga2 = new GroupAccount();

		ga1.GroupID = new int[] { account1.AccountID, account3.AccountID };
		ga1.AccountID = account2.AccountID;

		ga2.GroupID = new int[] { account1.AccountID, account2.AccountID, account3.AccountID };
		ga2.AccountID = account3.AccountID;

		// Type Question
		TypeQuestion tq1 = new TypeQuestion();
		TypeQuestion tq2 = new TypeQuestion();

		tq1.typeID = 1;
		tq1.typeName = "Essay";
		tq2.typeID = 2;
		tq2.typeName = "Multiple-Choice";

		// CategoryQuestion
		CategoryQuestion cq1 = new CategoryQuestion();
		CategoryQuestion cq2 = new CategoryQuestion();
		CategoryQuestion cq3 = new CategoryQuestion();

		cq1.CategoryID = 1;
		cq1.CategoryName = "Java";

		cq2.CategoryID = 2;
		cq2.CategoryName = "C++";

		cq3.CategoryID = 3;
		cq3.CategoryName = "C#";

		// Question
		Question q1 = new Question();
		Question q2 = new Question();
		Question q3 = new Question();

		q1.QuestionID = 1;
		q1.Content = "Content 1";
		q1.CategoryID = cq1.CategoryID; // Cau hoi 1 co kieu cau hoi la 1
		q1.TypeID = tq1.typeID; // Kieu essay
		q1.CreatorID = account2.AccountID; // Tao boi account 2
		q1.CreateDate = new Date();

		q2.QuestionID = 2;
		q2.Content = "Content 2";
		q2.CategoryID = cq2.CategoryID; // Cau hoi 2 co kieu cau hoi la 2
		q2.TypeID = tq1.typeID; // Kieu essay
		q2.CreatorID = account1.AccountID; // Tao boi account 1
		q2.CreateDate = new Date();

		q3.QuestionID = 3;
		q3.Content = "Content 1";
		q3.CategoryID = cq3.CategoryID; // Cau hoi 3 co kieu cau hoi la 3
		q3.TypeID = tq2.typeID; // Kieu Mutiple-Choice
		q3.CreatorID = account3.AccountID; // Tao boi account 3
		q3.CreateDate = new Date();

		// Answer
		Answer as1 = new Answer();
		Answer as2 = new Answer();
		Answer as3 = new Answer();

		as1.AnswerID = 1;
		as1.Content = "Content Answer 1";
		as1.QuestionID = q1.CategoryID; // Tra loi 1 ung voi hoi 1
		as1.isCorrect = true;

		as2.AnswerID = 2;
		as2.Content = "Content Answer 2";
		as2.QuestionID = q2.CategoryID; // Tra loi 1 ung voi hoi 2
		as2.isCorrect = false;

		as3.AnswerID = 3;
		as3.Content = "Content Answer 3";
		as3.QuestionID = q3.CategoryID; // Tra loi 1 ung voi hoi 3
		as3.isCorrect = true;

//		Exam
		Exam ex1 = new Exam();
		Exam ex2 = new Exam();
		Exam ex3 = new Exam();

		ex1.ExamID = 1;
		ex1.Code = "Code 1";
		ex1.Title = "Title 1";
		ex1.CategoryID = cq1.CategoryID;
		ex1.Duration = 120;
		ex1.CreatorID = account1.AccountID;
		ex1.CreateDate = new Date();

		ex2.ExamID = 2;
		ex2.Code = "Code 2";
		ex2.Title = "Title 2";
		ex2.CategoryID = cq2.CategoryID;
		ex2.Duration = 80;
		ex2.CreatorID = account2.AccountID;
		ex2.CreateDate = new Date();

		ex3.ExamID = 3;
		ex3.Code = "Code 3";
		ex3.Title = "Title 3";
		ex3.CategoryID = cq3.CategoryID;
		ex3.Duration = 60;
		ex3.CreatorID = account1.AccountID;
		ex3.CreateDate = new Date();

		// ExamQuestion
		ExamQuestion eq1 = new ExamQuestion();
		ExamQuestion eq2 = new ExamQuestion();
		ExamQuestion eq3 = new ExamQuestion();

		eq1.ExamID = ex1.ExamID;
		eq1.QuestionID = q1.QuestionID;

		eq2.ExamID = ex2.ExamID;
		eq2.QuestionID = q2.QuestionID;

		eq3.ExamID = ex3.ExamID;
		eq3.QuestionID = q3.QuestionID;

////		Q1:
//		System.out.println("Q1:");
//		if (account2.DepartmentID != 0) {
//			System.out.println("Phòng ban của nhân viên này là: " + account2.DepartmentID);
//		} else {
//			System.out.println("Nhân viên này chưa có phòng ban");
//		}
//
////		Q2:
//		System.out.println("Q2:");
//
////		Q3:
//		System.out.println("Q3:");
//		System.out.println((account2.DepartmentID == 0) ? "Nhân viên này chưa có phòng ban"
//				: "Phòng ban của nhân viên này là: " + account2.DepartmentID);
//
////		Q4:
//		System.out.println("Q4:");
//		System.out.println((account1.PositionID != 0) ? "Account 1 da co vi tri" : "Account 1 chua co vi tri");
//		System.out.println((account1.PositionID == 1) ? "Day la Developer" : "Day khong la Developer");
//
////    	Q5:
//		System.out.println("Q5:");
//		int size = ga1.GroupID.length;
//		switch (size) {
//		case 1:
//			System.out.println("Nhom co 1 thanh vien");
//			break;
//		case 2:
//			System.out.println("Nhom co 2 thanh vien");
//			break;
//		case 3:
//			System.out.println("Nhom co 3 thanh vien");
//			break;
//		default:
//			System.out.println("Nhom co nhieu thanh vien");
//		}
//
////		Q6:
////		System.out.println("Q6:");
////		if () {
////			switch(size) {
////			case 1:
////				System.out.println();
////				break;
////			}
////		}
//
////		Q7:
//		System.out.println("Q7:");
//		int position = account1.PositionID;
//		switch (position) {
//		case 1:
//			System.out.println("Day la Dev");
//			break;
//		case 2:
//			System.out.println("Day la Test");
//			break;
//		case 3:
//			System.out.println("Day la PM");
//			break;
//		default:
//			System.out.println("Chua co vi tri");
//		}
//
////		Q8:
//		System.out.println("Q8:");
//		for (int i = 0; i < accounts.length; ++i) {
//			System.out.println("Email: " + accounts[i].Email);
//			System.out.println("Full Name: " + accounts[i].FullName);
//			System.out.println("Department: " + accounts[i].DepartmentID);
//		}
//
////		Q9:
//		System.out.println("Q9:");
//		for (int i = 0; i < departments.length; ++i) {
//			System.out.println(departments[i].DepartmentID);
//			System.out.println(departments[i].departmentName);
//
//		}
//
////		Q10:
//		System.out.println("Q10: ");
//		for (int i = 0; i < 2; ++i) {
//			System.out.println(accounts[i].Email);
//			System.out.println(accounts[i].FullName);
//			if (accounts[i].DepartmentID == 1) {
//				System.out.println(DepartmentName.Sale);
//			} else if (accounts[i].DepartmentID == 2) {
//				System.out.println(DepartmentName.Engineering);
//			} else if (accounts[i].DepartmentID == 3) {
//				System.out.println(DepartmentName.Marketting);
//			}
////			
//		}
//
////		Q13:
//		System.out.println("Q13: ");
//		for (int i = 0; i < accounts.length; ++i) {
//			if (accounts[i].AccountID == 2) {
//				continue;
//			}
//			System.out.println(accounts[i].AccountID);
//			System.out.println(accounts[i].Email);
//			System.out.println(accounts[i].FullName);
//			System.out.println(accounts[i].DepartmentID);
//		}
////		Q14:
//		System.out.println("Q14: ");
//		for (int i = 0; i < accounts.length; ++i) {
//			if (accounts[i].AccountID < 4) {
//				System.out.println(accounts[i].AccountID);
//				System.out.println(accounts[i].Email);
//				System.out.println(accounts[i].FullName);
//				System.out.println(accounts[i].DepartmentID);
//			}
//		}
//
////		Q15:
//		System.out.println("Q15: ");
//		for (int i = 0; i <= 20; ++i) {
//			if (i % 2 == 0) {
//				System.out.println(i);
//			}
//		}
		
		
		Exercise3 ex31 = new Exercise3();
		//ex31.question1(ex1);
		ex31.question2(ex1);
		
		
	}

}
